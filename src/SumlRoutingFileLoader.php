<?php

namespace Avris\Suml\Symfony;

use Avris\Suml\Suml;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RouteCollection;

class SumlRoutingFileLoader extends YamlFileLoader
{
    const FORMAT = 'suml';

    /** @var Suml */
    private $suml;

    public function __construct(FileLocatorInterface $locator, Suml $suml)
    {
        parent::__construct($locator);

        $this->suml = $suml;
    }

    public function load($file, $type = null)
    {
        $path = $this->locator->locate($file);

        $parsedConfig = $this->suml->parseFile($file);

        $collection = new RouteCollection();
        $collection->addResource(new FileResource($path));

        if (empty($parsedConfig)) {
            return $collection;
        }

        if (!is_array($parsedConfig)) {
            throw new \InvalidArgumentException(sprintf('The file "%s" must contain a SUML array.', $path));
        }

        foreach ($parsedConfig as $name => $config) {
            $this->validate($config, $name, $path);

            if (isset($config['resource'])) {
                $this->parseImport($collection, $config, $path, $file);
            } else {
                $this->parseRoute($collection, $name, $config, $path);
            }
        }

        return $collection;
    }

    public function supports($resource, $type = null)
    {
        return is_string($resource)
            && pathinfo($resource, PATHINFO_EXTENSION) === self::FORMAT
            && (!$type || $type === self::FORMAT);
    }
}