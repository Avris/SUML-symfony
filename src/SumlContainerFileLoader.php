<?php

namespace Avris\Suml\Symfony;

use Avris\Suml\Suml;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class SumlContainerFileLoader extends YamlFileLoader
{
    const FORMAT = 'suml';

    /** @var Suml */
    private $suml;

    public function __construct(ContainerBuilder $container, FileLocatorInterface $locator, Suml $suml)
    {
        parent::__construct($container, $locator);

        $this->suml = $suml;
    }

    protected function loadFile($file)
    {
        return $this->validate($this->suml->parseFile($file), $file);
    }

    public function supports($resource, $type = null)
    {
        return is_string($resource)
            && pathinfo($resource, PATHINFO_EXTENSION) === self::FORMAT
            && (!$type || $type === self::FORMAT);
    }

    private function validate($content, $file)
    {
        if (empty($content)) {
            return null;
        }

        if (!is_array($content)) {
            throw new InvalidArgumentException(sprintf('The service file "%s" is not valid. It should contain an array. Check your SUML syntax.', $file));
        }

        foreach ($content as $namespace => $data) {
            if (in_array($namespace, ['imports', 'parameters', 'services'])) {
                continue;
            }

            if (!$this->container->hasExtension($namespace)) {
                $extensionNamespaces = array_filter(array_map(
                    function (ExtensionInterface $ext) { return $ext->getAlias(); },
                    $this->container->getExtensions())
                );

                throw new InvalidArgumentException(sprintf(
                    'There is no extension able to load the configuration for "%s" (in %s). Looked for namespace "%s", found %s',
                    $namespace,
                    $file,
                    $namespace,
                    $extensionNamespaces ? sprintf('"%s"', implode('", "', $extensionNamespaces)) : 'none'
                ));
            }
        }

        return $content;
    }
}