<?php

namespace Avris\Suml\Symfony\DependencyInjection;

use Avris\Suml\Suml;
use Avris\Suml\Symfony\SumlContainerFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class SumlExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new SumlContainerFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config'),
            new Suml()
        );

        $loader->load('services.suml');
    }
}
