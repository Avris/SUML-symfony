<?php

namespace Avris\Suml\Symfony\Command;

use Avris\Suml\Suml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\OutputStyle;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class SumlFromYamlCommand extends Command
{
    protected static $defaultName = 'suml:from-yaml';

    /** @var Suml */
    private $suml;

    public function __construct(Suml $suml)
    {
        parent::__construct(null);
        $this->suml = $suml;
    }

    protected function configure()
    {
        $this
            ->setDescription('Convert a .yaml file to .suml')
            ->addArgument('input', InputArgument::REQUIRED, 'File or directory name')
            ->addOption('remove-original', 'r', InputOption::VALUE_NONE, 'Remove the original file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $removeOriginal = $input->getOption('remove-original');
        if ($removeOriginal) {
            $io->warning('The original YAML files will be removed');
        }

        if (!class_exists('Symfony\Component\Yaml\Yaml')) {
            $io->error('In order to convert YAML to SUML you need to run `composer require symfony:yaml`');
            return;
        }

        $inputPath = getcwd() . DIRECTORY_SEPARATOR . $input->getArgument('input');
        if (!is_dir($inputPath)) {
            $this->convert($inputPath, $io, $removeOriginal);
            return;
        }

        $files = (new Finder())->files()->in($inputPath)->name('*.yaml')->name('*.yml');
        foreach ($files as $file) {
            $this->convert($file->getPathname(), $io, $removeOriginal);
        }
        
        return 0;
    }

    private function convert(string $inputPath, OutputStyle $io, bool $removeOriginal)
    {
        $outputPath = preg_replace('#(?:\.ya?ml)?$#', '.suml', $inputPath, 1);

        $io->note(sprintf('Converting %s to %s', $inputPath, $outputPath));

        $data = Yaml::parseFile($inputPath);

        file_put_contents($outputPath, $this->suml->dump($data));

        if ($removeOriginal) {
            unlink($inputPath);
        }

        $io->success(sprintf('SUML written to %s', $outputPath));
    }
}
