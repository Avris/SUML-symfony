<?php

namespace Avris\Suml\Symfony;

use Avris\Suml\Suml;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

class SumlEncoder implements EncoderInterface, DecoderInterface
{
    const FORMAT = 'suml';

    /** @var Suml */
    private $suml;

    public function __construct(Suml $suml)
    {
        $this->suml = $suml;
    }

    public function encode($data, $format, array $context = [])
    {
        return $this->suml->dump($data);
    }

    public function supportsEncoding($format)
    {
        return $format === self::FORMAT;
    }

    public function decode($data, $format, array $context = [])
    {
        return $this->suml->parse($data);
    }

    public function supportsDecoding($format)
    {
        return $format === self::FORMAT;
    }
}