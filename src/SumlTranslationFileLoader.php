<?php

namespace Avris\Suml\Symfony;

use Avris\Suml\Exception\ParseException;
use Avris\Suml\Suml;
use Symfony\Component\Translation\Exception\InvalidResourceException;
use Symfony\Component\Translation\Loader\FileLoader;


class SumlTranslationFileLoader extends FileLoader
{
    /** @var Suml */
    private $suml;

    public function __construct(Suml $suml)
    {
        $this->suml = $suml;
    }

    protected function loadResource($resource)
    {
        try {
            return $this->suml->parseFile($resource);
        } catch (ParseException $e) {
            throw new InvalidResourceException(sprintf('Error parsing YAML, invalid file "%s"', $resource), 0, $e);
        }
    }
}