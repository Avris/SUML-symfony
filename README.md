# SUML support for Symfony

![SUML logo](logo.png)

This is a Symfony bundle that provides a support for SUML: Simple & Unambiguous Markup Language.
You can check out its specification at [gitlab.com/Avris/SUML](https://gitlab.com/Avris/SUML)

## Features

 - Registers `Avris\Suml\Suml` as a service
 - Adds `'suml'` as a type for [The Serializer Component](https://symfony.com/doc/current/components/serializer.html)
 - Adds `.suml` files support for [The Routing Component](https://symfony.com/doc/current/components/routing.html)
 - Adds `.suml` files support for [The DependencyInjection Component](https://symfony.com/doc/current/components/dependency_injection.html)
 - Adds `.suml` files support for [The Translation Component](https://symfony.com/doc/current/components/translation.html)
 - Adds `suml:from-yaml` command which converts YAML files to SUML

## Installation

    composer require avris/suml-symfony

In the Kernel of your application, add `.suml` as a valid config extension:

    const CONFIG_EXTS = '.{php,xml,yaml,yml,suml}';
    
And activate SUML as a source of Container config by adding a trait to your Kernel:

    use SumlKernelTrait;
    
If you want to convert your existing files from YAML to SUML, you can run:

    bin/console suml:from-yaml <directory-or-filename> -r
